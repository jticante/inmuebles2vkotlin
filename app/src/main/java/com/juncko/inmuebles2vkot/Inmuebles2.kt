package com.juncko.inmuebles2vkot

import android.app.Application
import com.juncko.inmuebles2vkot.di.networkingModule
import com.juncko.inmuebles2vkot.ui.login.di.LoginModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class Inmuebles2 : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@Inmuebles2)
            modules(listOf(networkingModule,LoginModule))
        }

        if (BuildConfig.DEBUG) System.setProperty("kotlinx.coroutines.debug", "on")
    }
}