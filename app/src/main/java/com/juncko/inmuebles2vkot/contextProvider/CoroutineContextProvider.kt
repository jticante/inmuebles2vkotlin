package com.juncko.inmuebles2vkot.contextProvider

import kotlin.coroutines.CoroutineContext

interface CoroutineContextProvider {
    fun context(): CoroutineContext
}