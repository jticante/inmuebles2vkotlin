package com.juncko.inmuebles2vkot.contextProvider

import kotlin.coroutines.CoroutineContext

class CoroutineContextProviderImpl(
    private val context: CoroutineContext
) : CoroutineContextProvider {
    override fun context(): CoroutineContext = context

}