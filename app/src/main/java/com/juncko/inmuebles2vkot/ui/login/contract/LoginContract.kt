package com.juncko.inmuebles2vkot.ui.login.contract

import com.juncko.inmuebles2vkot.data.model.UserRequest
import com.juncko.inmuebles2vkot.data.model.UserResponse


interface LoginContract {
    interface View {
        fun showError(error : String)
        fun showLoader(show : Boolean)
        fun responseLogin(login : UserResponse)
    }

    interface Presenter {
        fun postLogin( request: UserRequest)
    }

    interface Repository {
        suspend fun postLogin(request: UserRequest): UserResponse
    }
}