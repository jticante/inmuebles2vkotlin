package com.juncko.inmuebles2vkot.ui.login.di

import com.juncko.inmuebles2vkot.contextProvider.CoroutineContextProvider
import com.juncko.inmuebles2vkot.contextProvider.CoroutineContextProviderImpl
import com.juncko.inmuebles2vkot.ui.login.LoginPresenterImpl
import com.juncko.inmuebles2vkot.ui.login.LoginRepositoryImpl
import com.juncko.inmuebles2vkot.ui.login.contract.LoginContract
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val LoginModule = module {
    single { CoroutineContextProviderImpl(Dispatchers.IO) as CoroutineContextProvider }
    single<LoginContract.Repository> { LoginRepositoryImpl(get()) }
    factory<LoginContract.Presenter> { (lv : LoginContract.View) ->
        LoginPresenterImpl(get(), lv, get())
    }
}