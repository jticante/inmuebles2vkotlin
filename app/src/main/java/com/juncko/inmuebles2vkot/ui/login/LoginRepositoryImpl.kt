package com.juncko.inmuebles2vkot.ui.login

import android.util.Log
import com.juncko.inmuebles2vkot.data.api.InmueblesApiService
import com.juncko.inmuebles2vkot.data.model.UserRequest
import com.juncko.inmuebles2vkot.data.model.UserResponse
import com.juncko.inmuebles2vkot.ui.login.contract.LoginContract

class LoginRepositoryImpl(private val inmueblesApiService: InmueblesApiService ) : LoginContract.Repository{
    override suspend fun postLogin(request: UserRequest): UserResponse {
        val userResponse = try {
            inmueblesApiService.postLogin(request)
        }catch (error: Throwable) {
            Log.e("Error Login", "${error.localizedMessage}")
            null
        }
        return userResponse!!
    }
}