package com.juncko.inmuebles2vkot.ui.login.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.juncko.inmuebles2vkot.R
import com.juncko.inmuebles2vkot.data.model.UserRequest
import com.juncko.inmuebles2vkot.data.model.UserResponse
import com.juncko.inmuebles2vkot.ui.login.contract.LoginContract
import com.juncko.inmuebles2vkot.ui.navHost.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class LoginActivity : AppCompatActivity(), LoginContract.View {
    val loginPresenter : LoginContract.Presenter by inject { parametersOf(this) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener {
            val usuario = this.edtEmail.text.toString().trim()
            val password = this.edtPassword.text.toString().trim()
            val requestLogin  = UserRequest(usuario,password)
            loginPresenter.postLogin(requestLogin)
        }
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoader(show: Boolean) {

    }

    override fun responseLogin(login: UserResponse) {
        if (login.estatus == true){
            val intent = Intent(this, MainActivity::class.java )
            startActivity(intent)
            finish()
        } else{
            Toast.makeText(this, login.mensaje, Toast.LENGTH_LONG).show()
        }
    }
}
