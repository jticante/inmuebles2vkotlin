package com.juncko.inmuebles2vkot.ui.navHost

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.juncko.inmuebles2vkot.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
