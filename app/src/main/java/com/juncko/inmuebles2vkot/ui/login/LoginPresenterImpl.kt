package com.juncko.inmuebles2vkot.ui.login

import android.content.Context
import com.juncko.inmuebles2vkot.data.model.UserRequest
import com.juncko.inmuebles2vkot.ui.login.contract.LoginContract
import kotlinx.coroutines.*

class LoginPresenterImpl(val context: Context,val view : LoginContract.View,val repository : LoginContract.Repository) : LoginContract.Presenter {
    private val TAG : String = LoginPresenterImpl::class.java.simpleName
    var job : Job? = null

    val exceptionHandler = CoroutineExceptionHandler{
            coroutineContext, throwable -> throwable.printStackTrace()
    }

    override fun postLogin(request: UserRequest) {
        view.showLoader(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val result = runCatching {
                repository.postLogin(request)
            }
            withContext(Dispatchers.Main){
                result.onSuccess { userResponse ->
                    view.responseLogin(userResponse)
                    view.showLoader(false)
                }.onFailure { error ->
                    handleError(error)
                    view.showLoader(false)
                }
            }
        }
    }

    private fun handleError(throwable: Throwable) {
        view.showError(throwable.localizedMessage)
    }
}