package com.juncko.inmuebles2vkot.di

import com.juncko.inmuebles2vkot.data.api.InmueblesApiService
import com.juncko.inmuebles2vkot.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


fun providesHttpClient() : OkHttpClient {

    val interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    return OkHttpClient().newBuilder().connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS).addInterceptor(interceptor).addInterceptor {
        var request = it.request()
        val builder = request.newBuilder().addHeader("Content-Type","application/json").method(request.method, request.body)
        it.proceed(builder.build())
    }.build()
}

fun providesRetrofit(httpClient: OkHttpClient) : Retrofit {
    return Retrofit.Builder()
        .baseUrl(Constants.BASEURL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

val networkingModule = module {
    single { providesHttpClient() }
    single { providesRetrofit(get())}
    single { get<Retrofit>().create(InmueblesApiService::class.java) }
}