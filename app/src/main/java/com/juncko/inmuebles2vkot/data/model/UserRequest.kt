package com.juncko.inmuebles2vkot.data.model

import com.google.gson.annotations.SerializedName

data class UserRequest (
    @field:SerializedName("Usuario")
    val usuario: String? = null,
    @field:SerializedName("Password")
    val password: String? = null
)