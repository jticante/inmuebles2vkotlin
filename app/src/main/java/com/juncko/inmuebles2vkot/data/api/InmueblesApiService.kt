package com.juncko.inmuebles2vkot.data.api


import com.juncko.inmuebles2vkot.data.model.UserRequest
import com.juncko.inmuebles2vkot.data.model.UserResponse
import com.juncko.inmuebles2vkot.utils.Constants
import retrofit2.http.Body
import retrofit2.http.POST

interface InmueblesApiService {
    @POST(Constants.LOGIN)
    suspend fun postLogin(@Body userRequest: UserRequest): UserResponse
}