package com.juncko.inmuebles2vkot.data.model

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("UsuarioId")
    val usuarioId: String? = null,
    @SerializedName("Estatus")
    val estatus: Boolean? = null,
    @SerializedName("Mensaje")
    val mensaje: String? = null
)