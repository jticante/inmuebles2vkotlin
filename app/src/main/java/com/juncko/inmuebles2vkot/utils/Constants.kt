package com.juncko.inmuebles2vkot.utils

class Constants{
    companion object {
        const val BASEURL = "http://blueiconstocks.cloudapp.net:81/"
        const val CONNECTION_TIMEOUT = 60000L
        const val LOGIN = "api/CuentaUsuarios/LoginUsuario"
    }
}